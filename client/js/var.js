function width_height(width, height) {
    this.width      = width;
    this.height     = height;
}

function position(x, y) {
    this.position = {
        x: x,
        y: y,
    }
}

//after this period I will make them all class
function main() {
    this.fps        = 30;
    this.canvas     = document.getElementById("main_canvas");
    this.context    = this.canvas.getContext("2d");
    if (!this.canvas || !this.context)
        log_alert("canvas error!");
}

function map() {
    width_height.call(this, 10000, 10000);

    this.grid={
        interval    : 80,
        color       : "#0101011A",
    }

    this.redline={
        color       : "#FF0000FF",
    }
}

function car() {
    this.img={
        pic         : new Image(),
    }
    if (!this.img.pic)
        log_alert("img of car missing!");
    this.img.pic.src    = "car.png";

    width_height.call(this.img, main.canvas.width / 30, main.canvas.width * 1.618 / 30)
    position    .call(this, map.width / 2, map.height / 2);

    this.tail = {
        width           : this.img.width * 0.5,
        //the head of the queue
        head            : 0,
        sample_interval : 0,
        sample_num      : 40,
        data_queue      : new Array(),
    }
    for (var i = 0; i < this.tail.sample_num; ++i) {
        this.tail.data_queue.push({
            x: this.position.x,
            y: this.position.y
        });
    }

    this.direction      = 0;
    this.speed_max      = 20;
    this.speed_min      = 5;
    this.speed          = 5;

    this.b_speed_up     = false;

    this.turn_angle_max = Math.PI * 40;
}

function mouse() {
    position.call(this, 0, 0);
    this.direction          = 0;
    this.distance_square    = 0;
}

function camera() {
    position.call(this, car.position.x - main.canvas.width / 2, car.position.y - main.canvas.height / 2)
    this.speed_circle   = {
        radius          : Math.min(main.canvas.height / 5, main.canvas.width / 8),
        width           : 5,
        color           : "#FF00007F",
    }

    this.speed_up={
        fuel                : 1.0,
        speed_up_min        : 0.3,
        speed               : 15,
        color_boundary      : "#0FFFF07F",
    }
    width_height.call(this.speed_up, main.canvas.width / 3, main.canvas.height / 20);

    this.blood_bar={
        offset              : car.img.height,
        blood_value         : 1.0,
        color_blood         : "#00FF007F",
        color_boundary      : "#0000FF7F",
    }
    width_height.call(this.blood_bar, 2 * car.img.width, 10);

    this.small_map={
        color               : "#0101017F",
    }
    width_height.call(this.small_map, (main.canvas.height * map.width) / (3 * map.height), main.canvas.height / 3);
    //attention the 3 in the call up should be changed both
}

//currently abandoned
/*
var bullet = {
    small_bullet: {
        stack: new Array(),
        speed: 20,            //every small_bullet's speed bust be the same
        num: 5,
        precision: 0.1 * Math.PI,
    }
};
*/