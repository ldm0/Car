function hsv(i,total){
    //hsv
    var rgb     = "";

    //I set the s and v to the highest and lowest
    var hue     = (6 * (i / total.toFixed(1)));
    var hue_int = Math.floor(hue);
    var inc     = Math.round((hue - hue_int) * 255);
    switch (hue_int) {
        case 0:
            rgb = "#" + "FF" + zero_fill(inc) + "00";
            break;
        case 1:
            rgb = "#" + zero_fill(255 - inc) + "FF" + "00";
            break;
        case 2:
            rgb = "#" + "00" + "FF" + zero_fill(inc);
            break;
        case 3:
            rgb = "#" + "00" + zero_fill(255 - inc) + "FF";
            break;
        case 4:
            rgb = "#" + zero_fill(inc) + "00" + "FF";
            break;
        case 5:
            rgb = "#" + "FF" + "00" + zero_fill(255 - inc);
            break;
        default:
            console.log(hue + ',' + hue_int);
    }
    return rgb;
}

function alpha(i,total){
    return zero_fill(255.0*i/total);
}

//zero fill the num's hex string
function zero_fill(num){
    var result= Math.round(num).toString(16);
    if (result.length < 2)
        result="0"+result;
    return result;
}

function blood_color(blood_value){
    if(blood_value>1.1||blood_color<-0.1)
        log_alert("the blood value illigal!");
    var rgb = "#" + zero_fill(255 * Math.max(0, Math.min(3.5 - 4 * blood_value, 1))) + zero_fill(255 * Math.max(0, Math.min(1, 4 * blood_value - 1.5))) + "00AF";
    return rgb;
}