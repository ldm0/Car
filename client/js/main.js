//global init
init();

setInterval(function main_loop(event) {

    //clear the whole canvas
    main.context.clearRect(0, 0, main.canvas.width, main.canvas.height);

    //attention: put the move after the draw because of the bullet
    //however which will cause a little laggy experience (guess

    draw();

    move();

}, 1000 / main.fps)