function draw(){
    draw_grid();
    draw_tail();
    draw_car();
    draw_speed_circle();
    draw_blood_bar();
    draw_fuel_bar();
    draw_small_map();
    draw_coin();
    //draw_bullet();
}


function draw_grid(){
    for(var i=0;i<Math.max(main.canvas.width,main.canvas.height);++i){
        if (i < main.canvas.width) { 
            //vertical grid
            if (Math.abs(camera.position.x + i) % map.grid.interval < 10) {
                main.context.beginPath();
                main.context.lineWidth="1";
                main.context.strokeStyle=map.grid.color;
                main.context.moveTo(i,0);
                main.context.lineTo(i,main.canvas.height);
                main.context.stroke();
            }
            //vertical red line
            if (
                Math.abs(camera.position.x + i - map.width - main.canvas.width / 2) < 20
                ||
                Math.abs(camera.position.x + i - main.canvas.width / 2) < 20
            ) {
                main.context.beginPath();
                main.context.lineWidth="1";
                main.context.strokeStyle=map.redline.color;
                main.context.moveTo(i,0);
                main.context.lineTo(i,main.canvas.height);
                main.context.stroke();
            }
        }
        if (i < main.canvas.height){
            //horizontal grid
            if( Math.abs(camera.position.y + i) % map.grid.interval < 10) {
                main.context.beginPath();
                main.context.lineWidth="1";
                main.context.strokeStyle=map.grid.color;
                main.context.moveTo(0,i);
                main.context.lineTo(main.canvas.width,i);
                main.context.stroke();
            }
            //horizontal red line
            if (
                Math.abs(camera.position.y + i - map.height-main.canvas.height/2) < 20
                ||
                Math.abs(camera.position.y + i - main.canvas.height / 2) < 20
            ) {
                main.context.beginPath();
                main.context.lineWidth="1";
                main.context.strokeStyle=map.redline.color;
                main.context.moveTo(0,i);
                main.context.lineTo(main.canvas.width,i);
                main.context.stroke();
            }
        }
    }
}

function draw_fuel_bar(){
    //draw the border
    main.context.beginPath();
    main.context.moveTo(20 + camera.speed_up.width * camera.speed_up.speed_up_min,20);
    main.context.lineTo(20 + camera.speed_up.width * camera.speed_up.speed_up_min,20+camera.speed_up.height);
    main.context.rect(20, 20, camera.speed_up.width, camera.speed_up.height);
    main.context.lineWidth = "10";
    main.context.strokeStyle = camera.speed_up.color_boundary;
    main.context.stroke();

    //draw the fuel
    main.context.fillStyle="#FF00007F";
    main.context.fillRect(20, 20, camera.speed_up.fuel * camera.speed_up.width, camera.speed_up.height);
}

function draw_tail(){
    //beacause a circle is not what I want, so i<car.tail.sample_num-1
    for (var i = 0; i < car.tail.sample_num - 1; ++i) {
        

        main.context.beginPath();

        //the former is wider the later is slimmer
        //main.context.lineWidth = car.tail.width*i/car.tail.sample_num.toFixed(1);
        main.context.lineWidth = car.tail.width;

        if(car.b_speed_up)
            main.context.strokeStyle = "#FF5F00" + alpha(i, car.tail.sample_num);
        else
            main.context.strokeStyle = hsv((car.tail.head + i) % car.tail.sample_num, car.tail.sample_num) + alpha(i, car.tail.sample_num);

        main.context.moveTo(
            car.tail.data_queue[(car.tail.head + i) % car.tail.sample_num].x - camera.position.x,
            car.tail.data_queue[(car.tail.head + i) % car.tail.sample_num].y - camera.position.y,
        );
        main.context.lineTo(
            car.tail.data_queue[(car.tail.head + i + 1) % car.tail.sample_num].x - camera.position.x,
            car.tail.data_queue[(car.tail.head + i + 1) % car.tail.sample_num].y - camera.position.y,
        )
        //the problem is all the line have been drawn will be drawn again
        //main.context.strokeStyle="#00FF00";

        main.context.stroke();
    };
}

function draw_car(){
    main.context.translate(main.canvas.width / 2, main.canvas.height / 2);
        main.context.rotate(Math.PI / 2 + car.direction);
            main.context.translate(-car.img.width / 2, -car.img.height / 2)
                main.context.drawImage(car.img.pic, 0, 0, car.img.width, car.img.height);
            main.context.translate(car.img.width / 2, car.img.height / 2)
        main.context.rotate(-(Math.PI / 2 + car.direction));
    main.context.translate(-main.canvas.width / 2, -main.canvas.height / 2);
}

function draw_speed_circle(){
    main.context.beginPath();
    main.context.lineWidth = camera.speed_circle.width;
    main.context.arc(main.canvas.width / 2, main.canvas.height / 2, camera.speed_circle.radius, 0, 2 * Math.PI);
    main.context.strokeStyle=camera.speed_circle.color;
    main.context.stroke();
}

function draw_blood_bar(){
    //which is just a rect
    //draw the boundary
    main.context.beginPath();
    main.context.lineWidth      ="1";
    main.context.rect(
        (main.canvas.width  - camera.blood_bar.width ) / 2,
        (main.canvas.height - camera.blood_bar.height) / 2 + camera.blood_bar.offset,
        camera.blood_bar.width,
        camera.blood_bar.height,
    );
    main.context.strokeStyle    =camera.blood_bar.color_boundary;
    main.context.stroke();

    //draw the blood
    main.context.fillStyle=camera.blood_bar.color_blood;
    main.context.fillRect(
        (main.canvas.width  - camera.blood_bar.width ) / 2,
        (main.canvas.height - camera.blood_bar.height) / 2 + camera.blood_bar.offset,
        Math.max(0,camera.blood_bar.blood_value) * camera.blood_bar.width,
        camera.blood_bar.height,
    );
}

//currently abandoned
//unfinished
function draw_bullet(){
    console.log(bullet.small_bullet.stack[0].position.x);
    //using a short line to represent the small bullet 
    for(var i=0;i<bullet.small_bullet.stack.length;++i){
        main.context.beginPath();
        main.context.lineWidth      ="1";
        main.context.strokeStyle    ="#FF0000";
        main.context.moveTo(
            bullet.small_bullet.stack[i].position.x
            +
            Math.cos(bullet.small_bullet.stack[i].direction)*bullet.small_bullet.stack[i].journey
            ,
            bullet.small_bullet.stack[i].position.y
            +
            Math.sin(bullet.small_bullet.stack[i].direction)*bullet.small_bullet.stack[i].journey
        );
        main.context.moveTo(
            bullet.small_bullet.stack[i].position.x
            +
            Math.cos(bullet.small_bullet.stack[i].direction)*bullet.small_bullet.stack[i].journey
            +
            Math.cos()
            ,
            bullet.small_bullet.stack[i].position.y
            +
            Math.sin(bullet.small_bullet.stack[i].direction)*bullet.small_bullet.stack[i].journey
        );
        main.context.stroke();
    }
    //after draw, we move the bullet in case we dont know we shoot the bullet
    //no we move the bullet in the process of draw
}

function draw_small_map(){
    //draw the main small map
    main.context.fillStyle=camera.small_map.color;
    main.context.fillRect(0,main.canvas.height-camera.small_map.height,camera.small_map.width,camera.small_map.height);
    //draw the car sign
    main.context.beginPath();
    main.context.lineWidth='4';
    main.context.strokeStyle=camera.blood_bar.color_blood;
    main.context.arc(
        camera.small_map.width*camera.position.x/map.width,
        main.canvas.height-camera.small_map.height+camera.small_map.height*camera.position.y/map.height,
        1,
        0,
        2*Math.PI,
    );
    main.context.stroke();
}

function draw_coin(){
    main.context.beginPath();

    
}